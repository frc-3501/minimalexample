package org.usfirst.frc.team3501.robot.subsystems;

import org.usfirst.frc.team3501.robot.Constants;
import org.usfirst.frc.team3501.robot.MathLib;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SensorCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;

public class MotorExampleSubsystem extends Subsystem {
  private static final int MOTOR_PORT = 0;          // you set this number!!
  private static MotorExampleSubsystem elevator;

  private final WPI_TalonSRX yourMotor;

  private MotorExampleSubsystem() {
    yourMotor = new WPI_TalonSRX(MOTOR_PORT);
  }

  public static MotorExampleSubsystem getElevator() {
    if (elevator == null) {
      elevator = new MotorExampleSubsystem();
    }
    return elevator;
  }

  // Input is value between -1 (full reverse) to 1 (full forward).  Use 0 to stop the motor
  public void setMotorValue(double motorVal) {
    motorVal = MathLib.restrictToRange(motorVal, -1.0, 1.0);

    yourMotor.set(ControlMode.PercentOutput, motorVal);
  }

  public void stop() {
    setMotorValue(0);
  }
}
