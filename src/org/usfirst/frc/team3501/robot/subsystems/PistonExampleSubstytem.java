package org.usfirst.frc.team3501.robot.subsystems;

import org.usfirst.frc.team3501.robot.Constants;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SensorCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class PistonExampleSubstytem extends Subsystem {
    private static final int PISTON_PORT = 0;   // YOU NEED TO SET THIS NUMBER!!
    private static PistonExampleSubstytem myMechanism;

    private Solenoid intakeSolenoid;
    private boolean pistonActivated = false;

    private PistonExampleSubstytem() {
        this.intakeSolenoid = new Solenoid(PISTON_PORT);
    }

    public static PistonExampleSubstytem getMyMechanism() {
        if (myMechanism == null) {
            myMechanism = new PistonExampleSubstytem();
        }
        return myMechanism;
    }

    public boolean isPistonExtended() {
        return pistonActivated;
    }

    public void extendPiston() {
        this.pistonActivated = true;
        this.intakeSolenoid.set(true);
    }

    public void retractPiston() {
        this.pistonActivated = false;
        this.intakeSolenoid.set(false);
    }
}