package org.usfirst.frc.team3501.robot;

import org.usfirst.frc.team3501.robot.subsystems.DriveTrain;
import org.usfirst.frc.team3501.robot.subsystems.MotorExampleSubsystem;
import org.usfirst.frc.team3501.robot.subsystems.PistonExampleSubstytem;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Scheduler;

public class Robot extends IterativeRobot {
  private static final double FULL_FORWARD = 1;

  private static DriveTrain driveTrain;
  private static OI oi;
  private static MotorExampleSubsystem motorMechanism;
  private static PistonExampleSubstytem pistonMechanism;

  @Override
  public void robotInit() {
    driveTrain = DriveTrain.getDriveTrain();
    oi = OI.getOI();
    motorMechanism = MotorExampleSubsystem.getElevator();
  }

  @Override
  public void autonomousInit() {}

  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
    Scheduler.getInstance().removeAll();

    motorMechanism.setMotorValue(FULL_FORWARD);     // set motor to run full speed
  }

  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
  }

  public static MotorExampleSubsystem getMotorMechanism() {
    return MotorExampleSubsystem.getElevator();
  }

  public static DriveTrain getDriveTrain() {
    return DriveTrain.getDriveTrain();
  }

  public static OI getOI() {
    return OI.getOI();
  }

  public static PistonExampleSubstytem getPistonMechanism() {
    return PistonExampleSubstytem.getMyMechanism();
  }
}
