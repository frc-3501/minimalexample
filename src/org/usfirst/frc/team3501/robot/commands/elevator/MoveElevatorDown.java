package org.usfirst.frc.team3501.robot.commands.elevator;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.MotorExampleSubsystem;
import edu.wpi.first.wpilibj.command.Command;


public class MoveElevatorDown extends Command {

  private MotorExampleSubsystem elevator = Robot.getMotorMechanism();

  /**
   * @param target the height the elevator will move to in inches
   * @param maxTimeOut the maximum time this command will be allowed to run before being cut
   */
  public MoveElevatorDown() {
    requires(elevator);
  }

  @Override
  protected void initialize() {
    elevator.setCANTalonsCoast();
  }

  @Override
  protected void execute() {
    elevator.setMotorValue(-elevator.SPEED);
    System.out.println("MotorExampleSubsystem motor value: " + elevator.getMotorVal());
  }

  @Override
  protected boolean isFinished() {
    return elevator.isAtBottom();
  }

  @Override
  protected void end() {
    elevator.setCANTalonsBrake();
    this.elevator.stop();
    this.elevator.resetEncoders();
  }

  @Override
  protected void interrupted() {
    end();
  }
}
