package org.usfirst.frc.team3501.robot.commands.driving;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This command makes the robot drive a specified distance using encoders on the robot and using a
 * feedback loop
 *
 * parameters: distance the robot will move in inches motorVal: the motor input to set the motors to
 */
public class DriveSideways extends Command {
  private DriveTrain driveTrain = Robot.getDriveTrain();
  private double maxTimeOut;
  private double target;

  /**
   * @param distance: a positive value will cause the robot to move right, and a negative value will
   *        cause the robot to move left
   * @param maxTimeOut: the max time this command will be allowed to run on for
   */
  public DriveSideways(double distance, double maxTimeOut) {
    requires(driveTrain);
    this.maxTimeOut = maxTimeOut;
    this.target = distance;
    this.driveTrain.resetEncoders();
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    this.driveTrain.mecanumDrive(driveTrain.getRightLeftSpeed(), 0, 0);
  }

  @Override
  protected boolean isFinished() {
    return timeSinceInitialized() >= maxTimeOut
        || driveTrain.getRightLeftEncoderDistance() >= this.target;
  }

  @Override
  protected void end() {}

  @Override
  protected void interrupted() {
    end();
  }
}
