package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.PistonExampleSubstytem;
import edu.wpi.first.wpilibj.command.Command;

public class RunIntake extends Command {

  private PistonExampleSubstytem intake = Robot.getPistonMechanism();

  public RunIntake() {
    requires(intake);
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    intake.setMotorValues(-intake.outtakeSpeed);
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    Robot.getPistonMechanism().stop();
  }

  @Override
  protected void interrupted() {
    end();
  }
}
