package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.subsystems.PistonExampleSubstytem;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class ToggleIntakePiston extends Command {
  private PistonExampleSubstytem intake = PistonExampleSubstytem.getMyMechanism();

  public ToggleIntakePiston() {
    requires(intake);
  }

  @Override
  protected void initialize() {
    intake.extendPiston(!intake.isPistonExtended());
    System.out.println(intake.isPistonExtended());
  }

  @Override
  protected void execute() {

  }

  @Override
  protected boolean isFinished() {
    return timeSinceInitialized() > 1;
  }

  @Override
  protected void end() {
    intake.stop();
  }

  @Override
  protected void interrupted() {}
}
