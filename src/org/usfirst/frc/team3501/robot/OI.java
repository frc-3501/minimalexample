package org.usfirst.frc.team3501.robot;

import org.usfirst.frc.team3501.robot.commands.elevator.MoveElevatorDown;
import org.usfirst.frc.team3501.robot.commands.elevator.MoveElevatorUp;
import org.usfirst.frc.team3501.robot.commands.elevator.MoveToTarget;
import org.usfirst.frc.team3501.robot.commands.intake.MoveDown;
import org.usfirst.frc.team3501.robot.commands.intake.MoveUp;
import org.usfirst.frc.team3501.robot.commands.intake.RunIntake;
import org.usfirst.frc.team3501.robot.commands.intake.RunOuttake;
import org.usfirst.frc.team3501.robot.commands.intake.ToggleIntakeAngle;
import org.usfirst.frc.team3501.robot.commands.intake.ToggleIntakePiston;
import org.usfirst.frc.team3501.robot.subsystems.MotorExampleSubsystem;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

public class OI {
  private static OI oi;
  public Joystick ps4_controller;

  // PistonExampleSubstytem
  public Button runIntakeForward;
  public Button runIntakeBackward;
  public Button toggleIntakePiston;
  public Button toggleIntakeAngle;
  public static Button moveIntakeUp, moveIntakeDown;

  // MotorExampleSubsystem
  public static Button moveElevatorToBottom;
  public static Button moveElevatorToTop;
  public static Button moveElevatorUp;
  public static Button moveElevatorDown;



  public OI() {
    ps4_controller = new Joystick(Constants.OI.PS4_CONTROLLER_PORT);

    // PistonExampleSubstytem
    runIntakeForward =
        new JoystickButton(ps4_controller, Constants.OI.RUN_INTAKE_PORT);
    runIntakeForward.whenPressed(new RunIntake());

    runIntakeBackward =
        new JoystickButton(ps4_controller, Constants.OI.REVERSE_INTAKE_PORT);
    runIntakeBackward.whenPressed(new RunOuttake());

    toggleIntakePiston =
        new JoystickButton(ps4_controller, Constants.OI.INTAKE_PISTON_PORT);
    toggleIntakePiston.whenPressed(new ToggleIntakePiston());

    toggleIntakeAngle =
        new JoystickButton(ps4_controller, Constants.OI.TOGGLE_INTAKE_ANGLE);
    toggleIntakeAngle.whenPressed(new ToggleIntakeAngle());

    moveIntakeUp =
        new JoystickButton(ps4_controller, Constants.OI.MOVE_INTAKE_UP);
    moveIntakeUp.whileHeld(new MoveUp());

    moveIntakeDown =
        new JoystickButton(ps4_controller, Constants.OI.MOVE_INTAKE_DOWN);
    moveIntakeDown.whileHeld(new MoveDown());


    // MotorExampleSubsystem
    moveElevatorToBottom =
        new JoystickButton(ps4_controller, Constants.OI.ELEVATOR_TO_BOTTOM);
    moveElevatorToBottom.whenPressed(new MoveToTarget(MotorExampleSubsystem.BOTTOM_POS));

    moveElevatorToTop =
        new JoystickButton(ps4_controller, Constants.OI.ELEVATOR_TO_TOP);
    moveElevatorToBottom.whenPressed(new MoveToTarget(MotorExampleSubsystem.TOP_POS));

    moveElevatorUp =
        new JoystickButton(ps4_controller, Constants.OI.MOVE_ELEVATOR_UP);
    moveElevatorUp.whileHeld(new MoveElevatorUp());

    moveElevatorDown =
        new JoystickButton(ps4_controller, Constants.OI.MOVE_ELEVATOR_DOWN);
    moveElevatorDown.whileHeld(new MoveElevatorDown());


  }

  public static OI getOI() {
    if (oi == null)
      oi = new OI();
    return oi;
  }
}
